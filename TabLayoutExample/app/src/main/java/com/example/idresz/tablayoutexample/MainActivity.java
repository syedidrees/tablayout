package com.example.idresz.tablayoutexample;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.idresz.tablayoutexample.ApplicationConstants.ApplicationConstants;
import com.example.idresz.tablayoutexample.adapters.ViewPagerAdapter;
import com.example.idresz.tablayoutexample.fragments.DesignFragment;
import com.example.idresz.tablayoutexample.fragments.LogicFragment;
import com.example.idresz.tablayoutexample.fragments.NewsFragment;
import com.example.idresz.tablayoutexample.fragments.NothingFragment;
import com.example.idresz.tablayoutexample.fragments.PoliticsFragment;
import com.example.idresz.tablayoutexample.fragments.SportsFragment;
public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int outerPosition;
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                outerPosition = position;
                tabLayout.smoothScrollBy(((int)(tabLayout.getWidth() * (position / (double) ApplicationConstants.five))), ApplicationConstants.zero);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_DRAGGING){
                    tabLayout.smoothScrollBy(((int)(tabLayout.getWidth() * (outerPosition / (double) ApplicationConstants.five))), ApplicationConstants.zero);
                }
            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new NewsFragment(), getString(R.string.news));
        adapter.addFragment(new SportsFragment(), getString(R.string.sports));
        adapter.addFragment(new LogicFragment(), getString(R.string.logic));
        adapter.addFragment(new PoliticsFragment(), getString(R.string.politics));
        adapter.addFragment(new DesignFragment(), getString(R.string.design));
        adapter.addFragment(new NothingFragment(), getString(R.string.nothing));
        viewPager.setAdapter(adapter);
    }
}
